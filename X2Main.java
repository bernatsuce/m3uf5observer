package ExObserver;

public class X2Main {

	public static void main(String[] args) {
		X2Xat observable = new X2Xat();
        X2Filtre observer1 = new X2Filtre();
        X2Filtre observer2 = new X2Filtre();
        X2Filtre observer3 = new X2Filtre();
        
        observable.addPropertyChangeListener(observer1);
        observable.addPropertyChangeListener(observer2);
        observable.addPropertyChangeListener(observer3);

        observable.setXat("Visca Pepe Viyuela");
        System.out.println((observer2.getNom() == "Viyuela"));
        System.out.println((observer2.getNom() == "Pepe"));
        System.out.println((observer3.getNom() == "Rekkles"));
        
        observable.removePropertyChangeListener(observer3);
        
        X2Filtre observer4 = new X2Filtre();
        X2Filtre observer5 = new X2Filtre();
        X2Filtre observer6 = new X2Filtre();
        X2Filtre observer7 = new X2Filtre();
        X2Filtre observer8 = new X2Filtre();
        X2Filtre observer9 = new X2Filtre();
        
        observable.setXat("Jojopium es el millor jungla de Etiopia noJoke");
        System.out.println((observer4.getNom() == "Jojopium"));
        System.out.println((observer5.getNom() == "millor"));
        System.out.println((observer6.getNom() == "jungla"));
        System.out.println((observer7.getNom() == "Etiopia"));
        System.out.println((observer8.getNom() == "Mon"));
        
        System.out.println((observer9.getNom() == "noJoke"));
        System.out.println((observer9.getNom() == "Jojopium"));
        System.out.println((observer9.getNom() == "millor"));
        System.out.println((observer9.getNom() == "jungla"));
        System.out.println((observer9.getNom() == "Etiopia"));
        System.out.println((observer9.getNom() == "Mon"));
    }
}
