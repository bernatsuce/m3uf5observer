package ExObserver;

public class X1Main {
	public static void main(String[] args) {
		X1Player observable = new X1Player();
        X1Game observer1 = new X1Game();
        X1Game observer2 = new X1Game();
        X1Game observer3 = new X1Game();
        X1Game observer4 = new X1Game();
        X1Game observer5 = new X1Game();
        
        observable.addPropertyChangeListener(observer1);
        observable.addPropertyChangeListener(observer2);
        observable.addPropertyChangeListener(observer3);
        observable.addPropertyChangeListener(observer4);
        observable.addPropertyChangeListener(observer5);

        observable.setRecord("Red Bull", 432);
        System.out.println((observer1.getNom() == "Red Bull"));
        System.out.println((observer2.getNom() == "Red Bull"));
        System.out.println((observer3.getNom() == "Red Bull"));
        System.out.println((observer4.getNom() == "Red Bull"));
        System.out.println((observer5.getNom() == "Red Bull"));

        
        observable.removePropertyChangeListener(observer3);
        observable.setRecord("Pepe Viyuela", 520);
        System.out.println((observer1.getNom() == "Pepe Viyuela"));
        System.out.println((observer2.getNom() == "Pepe Viyuela"));
        System.out.println((observer3.getNom() == "Pepe Viyuela"));
        System.out.println((observer3.getNom() == "Red Bull"));
        System.out.println((observer4.getNom() == "Pepe Viyuela"));
        System.out.println((observer5.getNom() == "Pepe Viyuela"));
        
	}
}
