package ExObserver;

import java.beans.PropertyChangeEvent;;

public class X1Game {
	private String nom;


    public void propertyChange(PropertyChangeEvent evt) {
        this.setNom((String) evt.getNewValue());
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
    	this.nom = nom;
    }
}