package ExObserver;

import java.beans.PropertyChangeEvent;

public class X2Filtre {
	private String nomAFiltrar;


    public void propertyChange(PropertyChangeEvent evt) {
        this.setNom((String) evt.getNewValue());
    }

    public String getNom() {
        return nomAFiltrar;
    }

    public void setNom(String nom) {
    	this.nomAFiltrar = nom;
    }
}
