package ExObserver;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

public class X1Player {
    private String name;
	private int Record;
    private PropertyChangeSupport support;

    public X1Player() {
        support = new PropertyChangeSupport(this);
    }

    public void addPropertyChangeListener(X1Game observer1) {
        support.addPropertyChangeListener((PropertyChangeListener) observer1);
    }

    public void removePropertyChangeListener(X1Game observer3) {
        support.removePropertyChangeListener((PropertyChangeListener) observer3);
    }

    public void setRecord(String name, int newRecord) {
    	if(newRecord > this.Record) {
    		support.firePropertyChange("Player", this.Record, newRecord);
    		support.firePropertyChange("Player", this.name, name);
        	this.Record = newRecord;
        	this.name = name;
    	}
    }
}
